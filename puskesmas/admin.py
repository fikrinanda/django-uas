from django.contrib import admin
from .models import Puskesmas, Kecamatan

# Register your models here.


@admin.register(Puskesmas)
class PuskesmasAdmin(admin.ModelAdmin):
    pass


@admin.register(Kecamatan)
class KecamatanAdmin(admin.ModelAdmin):
    pass
