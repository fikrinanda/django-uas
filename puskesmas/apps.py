from django.apps import AppConfig


class PuskesmasConfig(AppConfig):
    name = 'puskesmas'
