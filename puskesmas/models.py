from django.db import models
from django.urls import reverse
# Create your models here.


class Puskesmas(models.Model):
    JENIS_CHOICES = (
        ('1', 'Non Rawat Inap'),
        ('2', 'Rawat Inap'),
    )

    nama = models.CharField("Nama Puskesmas", max_length=100)
    alamat = models.CharField("Alamat", max_length=200)
    kecamatan = models.CharField("Kecamatan", max_length=2)
    jenis = models.CharField("Jenis", max_length=2, choices=JENIS_CHOICES)

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('puskesmas')


class Kecamatan(models.Model):
    WILAYAH_CHOICES = (
        ('1', 'Kota'),
        ('2', 'Kabupaten'),
    )

    nama = models.CharField("Nama Kecamatan", max_length=100)
    wilayah = models.CharField(
        "Wilayah", max_length=2, choices=WILAYAH_CHOICES)

    def __srt__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('kecamatan')
