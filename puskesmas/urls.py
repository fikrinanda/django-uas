from django.urls import path
from .views import index, puskesmas, kecamatan, PuskesmasCreateView, KecamatanCreateView, PuskesmasUpdateView, PuskesmasDeleteView, login_view, logout_view, PuskesmasPDF, KecamatanPDF
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', index, name="home"),
    path('puskesmas', puskesmas, name="puskesmas"),
    path('kecamatan', kecamatan, name="kecamatan"),
    path('puskesmas/tambah', login_required(PuskesmasCreateView.as_view()),
         name="puskesmas_tambah"),
    path('kecamatan/tambah', login_required(KecamatanCreateView.as_view()),
         name="kecamatan_tambah"),
    path('puskesmas/ubah/<int:pk>',
         login_required(PuskesmasUpdateView.as_view()), name="puskesmas_ubah"),
    path('puskesmas/hapus/<int:pk>',
         login_required(PuskesmasDeleteView.as_view()), name="puskesmas_hapus"),
    path('login', login_view, name="login"),
    path('logout', logout_view, name="logout"),
    path('puskesmas/cetak', PuskesmasPDF.as_view(), name="puskesmas_cetak"),
    path('kecamatan/cetak', KecamatanPDF.as_view(), name="kecamatan_cetak"),
]
