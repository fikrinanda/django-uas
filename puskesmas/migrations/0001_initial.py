# Generated by Django 3.1.4 on 2020-12-19 12:46

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Puskesmas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=100, verbose_name='Nama Puskesmas')),
                ('alamat', models.CharField(max_length=200, verbose_name='Alamat')),
                ('kecamatan', models.CharField(max_length=2, verbose_name='Kecamatan')),
                ('jenis', models.CharField(choices=[('1', 'Non Rawat Inap'), ('2', 'Rawat Inap')], max_length=20, verbose_name='Jenis')),
            ],
        ),
    ]
