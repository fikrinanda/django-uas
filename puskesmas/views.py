from django.shortcuts import render, redirect
from .models import Puskesmas, Kecamatan
from django.views.generic import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .utils import Render
from django.views.generic import View

# Create your views here.

var = {}


def index(self):
    var = {'title': 'Home', }
    return render(self, 'index.html', context=var)


def puskesmas(self):
    var = {'title': 'Data Puskesmas', }
    var['puskesmas'] = Puskesmas.objects.values(
        'id', 'nama', 'alamat', 'kecamatan', 'jenis')
    var['kecamatan'] = Kecamatan.objects.values('id', 'nama', 'wilayah')
    return render(self, 'puskesmas/index.html', context=var)


def kecamatan(self):
    var = {'title': 'Data Kecamatan', }
    var['kecamatan'] = Kecamatan.objects.values('id', 'nama', 'wilayah')
    return render(self, 'kecamatan/index.html', context=var)


class PuskesmasCreateView(CreateView):
    model = Puskesmas
    fields = '__all__'
    template_name = 'puskesmas/tambah.html'

    def get_context_data(self, **kwargs):
        var = {'title': 'Tambah Data Puskesmas', }
        var['kecamatan'] = Kecamatan.objects.values('id', 'nama')
        context = var
        context.update(super().get_context_data(**kwargs))
        return context


class KecamatanCreateView(CreateView):
    model = Kecamatan
    fields = '__all__'
    template_name = 'kecamatan/tambah.html'

    def get_context_data(self, **kwargs):
        context = var
        context = {'title': 'Tambah Data Kecamatan', }
        context.update(super().get_context_data(**kwargs))
        return context


class PuskesmasUpdateView(UpdateView):
    model = Puskesmas
    fields = ['nama', 'alamat', 'kecamatan', 'jenis']
    template_name = 'puskesmas/ubah.html'

    def get_context_data(self, **kwargs):
        var = {'title': 'Ubah Data Puskesmas', }
        var['kecamatan'] = Kecamatan.objects.values('id', 'nama')
        context = var
        context.update(super().get_context_data(**kwargs))
        return context


class PuskesmasDeleteView(DeleteView):
    model = Puskesmas
    success_url = reverse_lazy('puskesmas')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context


def login_view(request):
    var = {'title': 'Login', }
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('home')
        else:
            return render(request, 'login.html', context=var)

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            return redirect('login')


@login_required
def logout_view(request):
    if request.method == "POST":
        if request.POST['logout'] == "Logout":
            print("logout")
            logout(request)

        return redirect('home')


class PuskesmasPDF(View):
    def get(self, request):
        var = {
            'puskesmas': Puskesmas.objects.values('id', 'nama', 'alamat', 'kecamatan', 'jenis'),
            'kecamatan': Kecamatan.objects.values('id', 'nama', 'wilayah'),
            'request': request
        }
        return Render.to_pdf(self, 'puskesmas/cetak.html', var)


class KecamatanPDF(View):
    def get(self, request):
        var = {
            'kecamatan': Kecamatan.objects.values('id', 'nama', 'wilayah'),
            'request': request
        }
        return Render.to_pdf(self, 'kecamatan/cetak.html', var)
